(function($) {
    console.log("FORM JS LOADED");
    
    $('#contact-form').submit(function(e){
        let data = $('#contact-form').serializeArray();
        let obj = {};
        data.forEach(element => {
            obj[element['name']] = element['value'];
        });
        console.log(obj);
        
        e.preventDefault();
        $.ajax({
            url:'/.netlify/functions/sendMail',
            type:'post',
            data:JSON.stringify(obj),
            success:function(){
                console.log("SUCCESS");
                $('#contact-form').trigger("reset");
            },
            error:function(e){
                alert("Não foi possível enviar o e-mail. Tente novamente mais tarde!")
                console.log("ERROR");
                console.log(e.responseText);
            }
        });
        console.log("SUBMIT TRIGGERED");
    });
})(jQuery);
'use strict'
// importing dependencies
import nodemailer from "nodemailer";

// Environment variables
const {
  MAIL_USER,
  MAIL_PASSWORD,
  MAIL_SENDER_DESCRIPTION,
  SMTP_HOST,
  SMTP_PORT
} = process.env;

exports.handler = async (event) => {
  const params = JSON.parse(event.body);
  console.log(params);
  
  const {
    name,
    from,
    subject,
    body
  } = params;

  let transporter = nodemailer.createTransport({
    host: SMTP_HOST,
    port: SMTP_PORT,
    secure: SMTP_PORT === 465,
    auth: {
      user: MAIL_USER,
      pass: MAIL_PASSWORD
    }
  });

  let info = await transporter.sendMail({
    from: MAIL_SENDER_DESCRIPTION,
    to: MAIL_SENDER_DESCRIPTION,
    subject,
    text: ["De: " + name, "Email: " + from, body].join('\n'),
  }).catch(e => e);

  return {
    statusCode: 200,
    body: JSON.stringify({
      info,
      subject,
      text: from + '\n' + body
    })
  };
};
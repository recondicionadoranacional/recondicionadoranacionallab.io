---
title: "Fundação"
description: "Com sólida experiência acumulada no setor de mecânica automotiva, os irmãos Zenon Veloso e Itamar Veloso fundaram, em 30 de abril de 1951, a Irmãos Veloso Ltda. Desde o início, a empresa demonstrou um compromisso inabalável com a qualidade e a inovação. Posteriormente, a razão social foi alterada para Retífica Nacional Ltda., consolidando sua identidade como referência em recondicionamento e retífica de motores."
image: "antiga_1.jpg"
historicalDate: "1951-04-30"
---
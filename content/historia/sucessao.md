---
title: "Sucessão"
description: "Desde 1990, Iarbas Veloso, filho de um dos fundadores, assumiu a liderança da Recondicionadora Nacional Ltda., dando continuidade ao legado de qualidade e compromisso da empresa. Sob sua gestão, a empresa fortaleceu suas operações e ampliou suas capacidades, sempre mantendo o cliente no centro de suas atividades."
image: "antiga_3.jpg"
historicalDate: "1990-05-02"
---